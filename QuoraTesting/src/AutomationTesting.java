import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.apache.log4j.Logger;

public class AutomationTesting {
	
		public static void main(String[] args) throws InterruptedException{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Personal_PC\\Desktop\\ST_Project\\Selenium\\SeleniumWebDriver\\chromedriver_win32\\chromedriver.exe");
			
			Logger logger;
			logger = Logger.getLogger("devpinoyLogger");
			
			logger.debug("Starting Selenium Tests");
			// TODO Auto-generated method stub
			WebDriver driver = new ChromeDriver();
			logger.debug("Opening Website");
			driver.get("http://www.phptravels.net/login");
			driver.manage().window().maximize();
			
			logger.debug("Starting Logging");
			if (Login(driver) == true) 
			{
				logger.debug("Login Passed");
				
				logger.debug("Updating Profile Information");
				
				if (UpdatingPersonalProfileInformation(driver) == true)
				{
					logger.debug("Perosnal Profile Updated Successfully");
				}
				else
				{
					logger.debug("Personal Profile Updation Failed");
				}
				
				if (BookingAndInvoicePreview(driver) == true)
				{
					logger.debug("Booking and Invoice Preview Successfull");
				}
				else
				{
					logger.debug("Booking and Invoice Preview Failed");;
				}
				
				if (PreviewFromWishList(driver) == true)
				{
					logger.debug("Preview From WishList Successfull");
				}
				else
				{
					logger.debug("Preview From WishList Failed");;
				}
				
				if (RemoveFromWishList(driver) == true)
				{
					logger.debug("Remove From WishList Successfull");
				}
				else
				{
					logger.debug("Remove From WishList Failed");;
				}

			}
			else
			{
				logger.debug("Login Failed");
			}
			
		}
		
		
		//Priority: 2
		static boolean RemoveFromWishList(WebDriver driver) throws InterruptedException
		{	
			boolean operationSuccess = true;
			try {
				driver.findElement(By.xpath("//a[@id='#wishlist']")).click();
				
				Thread.sleep(5000);
				WebElement remove = driver.findElement(By.xpath("//a[@id='12' and @class='btn btn-sm btn-block btn-danger removewish remove_btn']"));
				remove.click();
			}
			catch(Exception e)
			{
				operationSuccess = false;
			}
			
			if (operationSuccess == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		//Priority: 2
		static boolean PreviewFromWishList(WebDriver driver) throws InterruptedException
		{	
			boolean operationSuccess = true;
			try {
				driver.findElement(By.xpath("//a[@href='#wishlist']")).click();
				
				Thread.sleep(5000);
				WebElement preview = driver.findElement(By.xpath("//a[@href='https://www.phptravels.net/tours/thailand/bangkok/6-Days-Around-Thailand']"));
				preview.click();
			}
			catch(Exception e)
			{
				operationSuccess = false;
			}
			
			if (operationSuccess == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		//Priority: 2
		static boolean BookingAndInvoicePreview(WebDriver driver) throws InterruptedException
		{
			boolean operationSuccess = true;
			try {
				driver.findElement(By.xpath("//a[@href='#bookings']")).click();
				
				Thread.sleep(5000);
				WebElement invoice = driver.findElement(By.xpath("//a[@href='https://www.phptravels.net/invoice?id=193&amp;sessid=4279']"));
				invoice.click();
			}
			catch(Exception e)
			{
				operationSuccess = false;
			}
			
			if (operationSuccess == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		//Priority: 2
		static boolean UpdatingPersonalProfileInformation(WebDriver driver) throws InterruptedException
		{
			boolean operationSuccess = true;
			try {
				driver.findElement(By.xpath("//a[@href='#profile']")).click();
				
				Thread.sleep(5000);
				WebElement phone = driver.findElement(By.xpath("//input[@name='phone']"));
				phone.clear();
				phone.sendKeys("654321");
				
			    driver.findElement(By.xpath("//button[@class='btn btn-action btn-block updateprofile']")).click();
			}
			catch(Exception e)
			{
				operationSuccess = false;
			}
			
			if (operationSuccess == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		//Priority: 1
		static boolean Login(WebDriver driver) throws InterruptedException
		{
			driver.findElement(By.xpath("//input[@name='username' and @placeholder='Email']")).click();
			driver.findElement(By.xpath("//input[@name='username' and @placeholder='Email']")).sendKeys("user@phptravels.com");
			
			driver.findElement(By.xpath("//input[@name='password' and @placeholder='Password']")).click();
			driver.findElement(By.xpath("//input[@name='password' and @placeholder='Password']")).sendKeys("demouser");
			
			driver.findElement(By.xpath("//button[@class='btn btn-action btn-lg btn-block loginbtn']")).click();
			
			Thread.sleep(5000);
			System.out.println(driver.getCurrentUrl());
			if (driver.getCurrentUrl().contains("www.phptravels.net/account/"))
			{
				System.out.println("Login Passed");
				return true;
			}
			else
			{
				System.out.println("Login Failed");
				return false;
			}
		}
	}
